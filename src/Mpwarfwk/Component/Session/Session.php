<?php

    namespace Mpwarfwk\Component\Session;

class Session {



    public function __construct() {

        // Start session
        session_start();

    }

    // Set value for key
    public function setValue($key,$value) {

        $_SESSION[$key] = $value;
    }

    // Get value from key
    public function getValue($key) {

        return $_SESSION[$key];
    }

}