<?php

namespace Mpwarfwk\Component\Routing;

use Mpwarfwk\Component\Request\Request;

class Routing
{

    const PHP_ROUTES_CONFIG_FILE = '../src/Config/routes.php';
    private $request;

    public function __construct(Request $request)
    {
        $this->request = $request;

    }

    public function getRoute()
    {
        include '../src/Config/routes.php';

        // Get first argument to set controller. Other arguments will be gotten on the specific controller
        $arg_controller = $this->getFirstArgument();


        try {

            if ( $arg_controller != null ) {

                foreach ( $config['routes'] as $key ) {

                    if ( $key['url'] == $arg_controller ) {

                        return new Route( $key['controller'], $key['method']);
                    }
                }

                throw new \Exception( "Controller does not exist" );

            } else {

                // Error
                return new Route("Controller\\Home\\Home",'mainAction');
            }

        } catch (\Exception $error) {

            return new Route("Controller\\Error\\Error404",'mainAction');
        }



    }

    public function getFirstArgument() {

        $uri = $this->request->server->getValue( 'REQUEST_URI' );
        $uri_params = explode( '/', $uri );

        $arg_params = "default";

        // If there is not arguments, set / as controller
        if ($uri_params[1] == "") {

            $arg_controller = "/";

        } else {

            // Get controller and method
            $arg_controller = $uri_params[1];

            $params     = array_slice($uri_params, 2);
            $params     = array_filter($params);
            $arg_params = $params;
        }

        return $arg_controller;

    }
}