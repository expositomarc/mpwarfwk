<?php

namespace Mpwarfwk\Component\Templating;

interface Templating {

    public function render($template,$variables=null);
    public function assignVars($variables);
}