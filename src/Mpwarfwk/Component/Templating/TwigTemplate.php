<?php

namespace Mpwarfwk\Component\Templating;

use Twig_Environment;
use Twig_Loader_Filesystem;

class TwigTemplate implements Templating {

    public function __construct (){

        $loader = new Twig_Loader_Filesystem('/');
        $this->twig = new Twig_Environment($loader, array(
            'cache' => '../src/Templates/cache'
        ));
    }

    public function render( $template, $variables = null ) {

        $variables = array('name' => 'Fabien');
        $template = $this->twig->loadTemplate( $template );
        return $template->render( $variables );
    }

    public function assignVars($variables){

       // Nothing to do

    }
}